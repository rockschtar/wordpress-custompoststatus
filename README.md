# WordPress Custom Post Status

Library that implements custom post status in WordPress.

### Prerequisites

* PHP>=7.1
* [roots/bedrock](https://roots.io/bedrock/) based project

### Usage
plugin.php
```php
register_custom_post_status(
    'my-custom-post-status',
    array('my-custom-post-type'),
    array('label' => __('Some label', 'textdomain'),
        'public' => false,
        'private' => false,
        'internal' => false,
        'exclude_from_search' => true,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => true)
);
```