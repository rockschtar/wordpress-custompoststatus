<?php

namespace Deployer;

require 'recipe/common.php';
require 'vendor/rockschtar/deployer-wordpress-set-version/wp_set_version.php';

localhost()
    ->stage('build')
    ->roles('test', 'build')->set('deploy_path', __DIR__ . DIRECTORY_SEPARATOR . 'build');

set('ssh_type', 'native');
set('ssh_multiplexing', true);
set('repository','https://gitlab.com/rockschtar/wordpress-custompoststatus.git');
set('clear_paths', ['.git']);
set('composer_command', 'composer');
set('branch', 'develop');
set('default_stage', 'build');
set('set_version_files', ['wordpress-custompoststatus.php']);

//inventory('deploy.yml');



task('build', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'wordpress:set_version'
]);


// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
