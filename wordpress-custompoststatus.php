<?php
/**
 * Plugin Name: WordPress Custom Post Status
 * Plugin URI: https://gitlab.com/rockschtar/wordpress-custompoststatus
 * Description: Custom post status support for wordpress
 * Version: develop
 * Author: Stefan Helmer
 * Author URI: https://gitlab.com/rockschtar/
 * License: MIT License
 */

/**
 * @param String $post_status
 * @param array $post_types
 * @param array $settings
 */
function register_custom_post_status(String $post_status, array $post_types = ['post'], array $settings = []) {
    \Rockschtar\WordPress\CustomPostStatus\Controller\CustomPostStatusController::init($post_status, $post_types, $settings);
}