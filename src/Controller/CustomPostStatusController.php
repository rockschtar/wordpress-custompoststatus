<?php
namespace Rockschtar\WordPress\CustomPostStatus\Controller;

class CustomPostStatusController {
    private $post_status;
    private $post_types;
    private $settings;
    private $enable_action;
    private $default_settings = array(
        'label' => '',
        'public' => false,
        'internal' => false,
        'private' => false,
        'exclude_from_search' => false,
        'show_in_admin_all_list' => true,
        'show_in_admin_status_list' => false,
        'label_count' => ''
    );

    private function __construct(String $post_status, array $post_types, array $settings) {
        $this->post_status = $post_status;

        $this->post_types = $post_types;

        if (isset($settings['action']) && \in_array($settings['action'], array(false, true, 'publish', 'update'), true)) {
            $this->enable_action = $settings['action'];
        }
        if (!isset($settings['label']) || empty($settings['label'])) {
            $settings['label'] = ucfirst($settings['slug']);
        }
        if (!isset($args['label_count']) || empty($settings['label_count'])) {
            $settings['label_count'] = _n_noop($settings['label'] . ' <span class="count">(%s)</span>', $settings['label'] . ' <span class="count">(%s)</span>');
        }

        /** @noinspection UnusedConstructorDependenciesInspection */
        $this->settings = wp_parse_args($settings, $this->default_settings);

        add_action('init', array($this, 'register_post_status'));
        add_action('admin_footer', array($this, 'set_post_status'));
    }

    public static function init(String $post_status, array $post_types, array $settings): void {
        new self($post_status, $post_types, $settings);
    }

    public function register_post_status(): void {
        register_post_status($this->post_status, $this->settings);
    }

    public function set_post_status(): void {

        $set_status = apply_filters('rswpcps_custom_post_status_' . $this->post_status, true);

        if (!$set_status) {
            return;
        }

        global $post;

        if ($post === null) {
            return;
        }

        if (!\in_array($post->post_type, $this->post_types, true)) {
            return;
        }

        $complete = '';
        $label = '';

        if ($post->post_status === $this->post_status) {
            $complete = ' selected=\"selected\"';
            $label = '<span id=\"post-status-displaya\">' . $this->settings['label'] . '</span>';
        }
        ?>
        <script>
            (function ($) {
                $(document).ready(function () {
                    $('select#post_status').append("<option value='<?php echo $this->post_status; ?>' <?php echo $complete; ?>><?php echo $this->settings['label']; ?></option>");
                    $('.misc-pub-section #post-status-display').html("<?php echo $label; ?>");
                    <?php if ($complete !== '') {
                    // If the post has this status check the preferred action
                    // If true or 'publish', we leave it as default
                    if (!$this->enable_action) {
                        echo '$("#publish").remove();';
                    } elseif ($this->enable_action === 'update') {
                        echo '$("#publish").val("Update");$("#publish").attr("name","save");$("#original_publish").val("Update");';
                    }
                } ?>
                });
            })(jQuery);
        </script>
        <?php
    }
}